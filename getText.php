<?php

/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 18.05.2017
 * Time: 16:01
 */

/**Получение текста из файлов txt
 * Class getText
 */
class getText
{
    public static function getLink(){
        $link_array=file("text/link.txt");
        return $link_array;
    }
    public static function getNav(){
        $nav_array=file("text/nav.txt");
        return $nav_array;
    }
    public static function getModal(){
        $modal_array=file("text/modal.txt");
        return $modal_array;
    }
    public static function getWindows(){
        $windows_array=file("text/windows.txt");
        return $windows_array;
    }
}