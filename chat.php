<?php
/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 19.05.2017
 * Time: 10:44
 */
echo "<!DOCTYPE html>
<html>
<head>";
require_once("getText.php");
$link=getText::getLink();
foreach ($link as $item) {
    echo $item;
}
echo "<title>Чат</title>
</head>
<body>
<div class='container'>";
$nav=getText::getNav();
foreach ($nav as $item) {
    echo $item;
}
$modal=getText::getModal();
foreach ($modal as $item){
    echo $item;
}
$windows=getText::getWindows();
foreach ($windows as $item){
    echo $item;
}
echo "</div>
</body>
</html>";